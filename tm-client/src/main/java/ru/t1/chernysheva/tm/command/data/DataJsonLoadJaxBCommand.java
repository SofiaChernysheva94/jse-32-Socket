package ru.t1.chernysheva.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.request.data.DataLoadJsonFasterXmlRequest;
import ru.t1.chernysheva.tm.dto.request.data.DataLoadJsonJaxBRequest;
import ru.t1.chernysheva.tm.enumerated.Role;

public final class DataJsonLoadJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-json-jaxb";

    @NotNull
    private static final String DESCRIPTION  = "Load data from json.";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD JSON]");
        @NotNull final DataLoadJsonJaxBRequest request = new DataLoadJsonJaxBRequest();
        getDomainEndpoint().loadDataJsonJaxB(request);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
