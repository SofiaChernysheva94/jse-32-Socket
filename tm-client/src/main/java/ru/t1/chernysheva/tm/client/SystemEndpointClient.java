package ru.t1.chernysheva.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.api.endpoint.IAuthEndpointClient;
import ru.t1.chernysheva.tm.api.endpoint.ISystemEndpoint;
import ru.t1.chernysheva.tm.api.endpoint.ISystemEndpointClient;
import ru.t1.chernysheva.tm.dto.request.ApplicationAboutRequest;
import ru.t1.chernysheva.tm.dto.request.ApplicationVersionRequest;
import ru.t1.chernysheva.tm.dto.response.ApplicationAboutResponse;
import ru.t1.chernysheva.tm.dto.response.ApplicationVersionResponse;

@NoArgsConstructor
public final class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpointClient {

    @NotNull
    @Override
    public ApplicationAboutResponse getAbout(@NotNull ApplicationAboutRequest request) {
        return call(request, ApplicationAboutResponse.class);
    }

    @NotNull
    @Override
    public ApplicationVersionResponse getVersion(@NotNull ApplicationVersionRequest request) {
        return call(request, ApplicationVersionResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        @NotNull final ApplicationAboutResponse applicationAboutResponse = client.getAbout(new ApplicationAboutRequest());
        System.out.println(applicationAboutResponse.getAuthorEmail());
        System.out.println(applicationAboutResponse.getAuthorName());
        @NotNull final ApplicationVersionResponse applicationVersionResponse = client.getVersion(new ApplicationVersionRequest());
        System.out.println(applicationVersionResponse.getVersion());
        client.disconnect();
    }

}

