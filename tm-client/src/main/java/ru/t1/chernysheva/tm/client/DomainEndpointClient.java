package ru.t1.chernysheva.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.api.endpoint.IDomainEndpointClient;
import ru.t1.chernysheva.tm.dto.request.data.*;
import ru.t1.chernysheva.tm.dto.request.user.UserLoginRequest;
import ru.t1.chernysheva.tm.dto.request.user.UserLogoutRequest;
import ru.t1.chernysheva.tm.dto.response.data.*;

@NoArgsConstructor
public class DomainEndpointClient extends AbstractEndpointClient implements IDomainEndpointClient {

    public DomainEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    public DataLoadBackupResponse loadDataBackup(@NotNull DataLoadBackupRequest request) {
        return call(request, DataLoadBackupResponse.class);
    }

    @NotNull
    @Override
    public DataSaveBackupResponse saveDataBackup(@NotNull DataSaveBackupRequest request) {
        return call(request, DataSaveBackupResponse.class);
    }

    @NotNull
    @Override
    public DataLoadBase64Response loadDataBase64(@NotNull DataLoadBase64Request request) {
        return call(request, DataLoadBase64Response.class);
    }

    @NotNull
    @Override
    public DataSaveBase64Response saveDataBase64(@NotNull DataSaveBase64Request request) {
        return call(request, DataSaveBase64Response.class);
    }

    @NotNull
    @Override
    public DataLoadBinaryResponse loadDataBinary(@NotNull DataLoadBinaryRequest request) {
        return call(request, DataLoadBinaryResponse.class);
    }

    @NotNull
    @Override
    public DataSaveBinaryResponse saveDataBinary(@NotNull DataSaveBinaryRequest request) {
        return call(request, DataSaveBinaryResponse.class);
    }

    @NotNull
    @Override
    public DataLoadJsonFasterXmlResponse loadDataJsonFasterXml(@NotNull DataLoadJsonFasterXmlRequest request) {
        return call(request, DataLoadJsonFasterXmlResponse.class);
    }

    @NotNull
    @Override
    public DataLoadJsonJaxBResponse loadDataJsonJaxB(@NotNull DataLoadJsonJaxBRequest request) {
        return call(request, DataLoadJsonJaxBResponse.class);
    }

    @NotNull
    @Override
    public DataSaveJsonFasterXmlResponse saveDataJsonFasterXml(@NotNull DataSaveJsonFasterXmlRequest request) {
        return call(request, DataSaveJsonFasterXmlResponse.class);
    }

    @NotNull
    @Override
    public DataSaveJsonJaxBResponse saveDataJsonJaxB(@NotNull DataSaveJsonJaxBRequest request) {
        return call(request, DataSaveJsonJaxBResponse.class);
    }

    @NotNull
    @Override
    public DataLoadXmlFasterXmlResponse loadDataXmlFasterXml(@NotNull DataLoadXmlFasterXmlRequest request) {
        return call(request, DataLoadXmlFasterXmlResponse.class);
    }

    @NotNull
    @Override
    public DataLoadXmlJaxBResponse loadDataXmlJaxB(@NotNull DataLoadXmlJaxBRequest request) {
        return call(request, DataLoadXmlJaxBResponse.class);
    }

    @Override
    public @NotNull DataSaveJsonJaxBResponse saveDataXmlJaxB(@NotNull DataSaveJsonJaxBRequest request) {
        return call(request, DataSaveJsonJaxBResponse.class);
    }

    @NotNull
    @Override
    public DataSaveXmlFasterXmlResponse saveDataXmlFasterXml(@NotNull DataSaveXmlFasterXmlRequest request) {
        return call(request, DataSaveXmlFasterXmlResponse.class);
    }

    @NotNull
    @Override
    public DataSaveXmlJaxBResponse saveDataXmlJaxB(@NotNull DataSaveXmlJaxBRequest request) {
        return call(request, DataSaveXmlJaxBResponse.class);
    }

    @NotNull
    @Override
    public DataLoadYamlFasterXmlResponse loadDataYamlFasterXml(@NotNull DataLoadYamlFasterXmlRequest request) {
        return call(request, DataLoadYamlFasterXmlResponse.class);
    }

    @NotNull
    @Override
    public DataSaveYamlFasterXmlResponse saveDataYamlFasterXml(@NotNull DataSaveYamlFasterXmlRequest request) {
        return call(request, DataSaveYamlFasterXmlResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        {
            System.out.println(authEndpointClient.login(new UserLoginRequest("admin", "admin")).getSuccess());
            @NotNull final DomainEndpointClient domainClient = new DomainEndpointClient(authEndpointClient);
            domainClient.saveDataBase64(new DataSaveBase64Request());
        }
        {
            System.out.println(authEndpointClient.login(new UserLoginRequest("test", "test")).getSuccess());
            @NotNull final DomainEndpointClient domainClient = new DomainEndpointClient(authEndpointClient);
            domainClient.saveDataBase64(new DataSaveBase64Request());
        }
        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
        authEndpointClient.disconnect();
    }


}
