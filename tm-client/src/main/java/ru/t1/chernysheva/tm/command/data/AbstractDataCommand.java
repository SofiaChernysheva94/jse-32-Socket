package ru.t1.chernysheva.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.api.endpoint.IDomainEndpointClient;
import ru.t1.chernysheva.tm.api.service.IDomainService;
import ru.t1.chernysheva.tm.command.AbstractCommand;
import ru.t1.chernysheva.tm.dto.Domain;

public abstract class AbstractDataCommand extends AbstractCommand {

    public AbstractDataCommand() {
    }

    @NotNull
    public IDomainEndpointClient getDomainEndpoint() {
        return getServiceLocator().getDomainEndpointClient();
    }

}


