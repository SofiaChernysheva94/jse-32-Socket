package ru.t1.chernysheva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    void add(@Nullable AbstractCommand command);

    @Nullable
    AbstractCommand getCommandByArgument(@Nullable String argument);

    @Nullable
    AbstractCommand getCommandByName(@Nullable String name);

    @Nullable
    Iterable<AbstractCommand> getCommandsWithArgument();

    @NotNull
    Collection<AbstractCommand> getTerminalCommands();

}
