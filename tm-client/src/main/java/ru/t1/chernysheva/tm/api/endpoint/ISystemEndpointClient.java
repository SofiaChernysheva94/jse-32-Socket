package ru.t1.chernysheva.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.dto.request.ApplicationAboutRequest;
import ru.t1.chernysheva.tm.dto.request.ApplicationVersionRequest;
import ru.t1.chernysheva.tm.dto.response.ApplicationAboutResponse;
import ru.t1.chernysheva.tm.dto.response.ApplicationVersionResponse;

public interface ISystemEndpointClient extends IEndpointClient {

    @NotNull
    ApplicationAboutResponse getAbout(@NotNull ApplicationAboutRequest request);

    @NotNull
    ApplicationVersionResponse getVersion(@NotNull ApplicationVersionRequest request);

}
