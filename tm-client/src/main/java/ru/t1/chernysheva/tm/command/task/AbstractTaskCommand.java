package ru.t1.chernysheva.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.api.endpoint.ITaskEndpointClient;
import ru.t1.chernysheva.tm.command.AbstractCommand;
import ru.t1.chernysheva.tm.enumerated.Status;
import ru.t1.chernysheva.tm.model.Project;
import ru.t1.chernysheva.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    protected ITaskEndpointClient getTaskEndpoint() {
        return getServiceLocator().getTaskEndpointClient();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void renderTasks(final List<Task> tasks) {
        tasks.forEach(this::showTask);
    }

    protected void showTask(@Nullable final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

}
