package ru.t1.chernysheva.tm.client;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.api.endpoint.IEndpointClient;
import ru.t1.chernysheva.tm.dto.request.data.DataSaveJsonJaxBRequest;
import ru.t1.chernysheva.tm.dto.response.ApplicationErrorResponse;
import ru.t1.chernysheva.tm.dto.response.data.DataSaveJsonJaxBResponse;
import ru.t1.chernysheva.tm.model.User;

import java.io.*;
import java.net.Socket;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractEndpointClient implements IEndpointClient {

    private String host;

    private Integer port;

    private Socket socket;

    public AbstractEndpointClient(@NotNull final AbstractEndpointClient client) {
        this.host = client.host;
        this.port = client.port;
        this.socket = client.socket;
    }

    @NotNull
    @SneakyThrows
    protected <T> T call(@Nullable final Object data, @NotNull final Class<T> clazz) {
        getObjectOutputStream().writeObject(data);
        @NotNull final Object result = getObjectInputStream().readObject();
        if (result instanceof ApplicationErrorResponse) {
            @NotNull final ApplicationErrorResponse response = (ApplicationErrorResponse) result;
            throw new RuntimeException(response.getMessage());
        }
        return (T) result;
    }

    private ObjectOutputStream getObjectOutputStream() throws IOException {
        return new ObjectOutputStream(getOutputStream());
    }

    private ObjectInputStream getObjectInputStream() throws IOException {
        return new ObjectInputStream(getInputStream());
    }

    @Nullable
    private OutputStream getOutputStream() throws IOException {
        if (socket == null) return null;
        return socket.getOutputStream();
    }

    @Nullable
    private InputStream getInputStream() throws IOException {
        if (socket == null) return null;
        return socket.getInputStream();
    }

    @SneakyThrows
    @Override
    public Socket connect() {
        socket = new Socket("localhost", 6060);
        return socket;
    }

    @SneakyThrows
    @Override
    public void disconnect() {
        if (socket == null) return;
        socket.close();
    }

}

