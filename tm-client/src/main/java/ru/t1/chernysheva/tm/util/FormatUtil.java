package ru.t1.chernysheva.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.constant.MeasureConst;

import java.text.DecimalFormat;

public final class FormatUtil {

    @NotNull
    private static final String NAME_BYTES_SHORT = "B";

    @NotNull
    private static final String NAME_BYTES_LONG = "bytes";

    @NotNull
    private static final String NAME_KILOBYTE = "KB";

    @NotNull
    private static final String NAME_MEGABYTE = "MB";

    @NotNull
    private static final String NAME_GIGABYTE = "MB";

    @NotNull
    private static final String NAME_TERABYTE = "MB";

    @NotNull
    private static final String SEPARATOR = " ";

    @NotNull
    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.###");

    @NotNull
    private static String render(final double bytes) {
        return DECIMAL_FORMAT.format(bytes);
    }

    @NotNull
    private static String render(final long bytes, final double size) {
        return render(bytes / size);
    }

    @NotNull
    private static String render(final long bytes, final double size, @NotNull final String name) {
        return render(bytes, size) + SEPARATOR + name;
    }

    @NotNull
    private static String render(final long bytes, @NotNull final String name) {
        return render(bytes) + SEPARATOR + name;
    }

    @NotNull
    public static String formatBytes(final long bytes) {
        if ((bytes >= 0) && (bytes < MeasureConst.KB))
            return render(bytes, NAME_BYTES_SHORT);
        if ((bytes >= MeasureConst.KB) && (bytes < MeasureConst.MB)) return render(bytes, MeasureConst.KB, NAME_KILOBYTE);
        if ((bytes >= MeasureConst.MB) && (bytes < MeasureConst.GB)) return render(bytes, MeasureConst.MB, NAME_MEGABYTE);
        if ((bytes >= MeasureConst.GB) && (bytes < MeasureConst.TB)) return render(bytes, MeasureConst.GB, NAME_GIGABYTE);
        if (bytes >= MeasureConst.TB) return render(bytes, MeasureConst.GB, NAME_TERABYTE);
        return render(bytes, NAME_BYTES_LONG);
    }

}
