package ru.t1.chernysheva.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.dto.request.user.UserLoginRequest;
import ru.t1.chernysheva.tm.exception.system.AccessDeniedException;
import ru.t1.chernysheva.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "login";

    @NotNull
    private final String DESCRIPTION = "User login.";

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(login);
        request.setPassword(password);
        System.out.println(getAuthEndpoint().login(request).getMessage());
        if (!getAuthEndpoint().login(request).getSuccess())
            throw new AccessDeniedException();
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
