package ru.t1.chernysheva.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.dto.request.project.ProjectChangeStatusByIndexRequest;
import ru.t1.chernysheva.tm.enumerated.Status;
import ru.t1.chernysheva.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectChangeStatusByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private final String NAME = "project-change-status-by-index";

    @NotNull
    private final String DESCRIPTION = "Change project status by index.";

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = Status.toStatus(statusValue);

        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest();
        request.setIndex(index);
        request.setStatus(status);

        getProjectEndpoint().changeProjectStatusByIndex(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
