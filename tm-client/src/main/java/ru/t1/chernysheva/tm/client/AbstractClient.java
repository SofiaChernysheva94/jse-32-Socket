package ru.t1.chernysheva.tm.client;

import java.io.*;
import java.net.Socket;

public abstract class AbstractClient {

    public String host = "localhost";

    private Integer port = 6060;

    private Socket socket;

    public AbstractClient() {

    }

    public AbstractClient(String host, Integer port) {
        this.host = host;
        this.port = port;
    }

    protected Object call(final Object data) throws IOException, ClassNotFoundException {
        getObjectOutputStream().writeObject(data);
        return getObjectInputStream().readObject();
    }

    private ObjectOutputStream getObjectOutputStream() throws IOException {
        return new ObjectOutputStream(getOutputStream());
    }

    private InputStream getInputStream() throws IOException {
        return socket.getInputStream();
    }

    private ObjectInputStream getObjectInputStream() throws IOException {
        return new ObjectInputStream(getInputStream());
    }

    private OutputStream getOutputStream() throws IOException {
        return socket.getOutputStream();
    }

    public void connect() throws IOException {
        socket = new Socket(host, port);
    }

    public void disconnect() throws IOException {
        socket.close();
    }

}
