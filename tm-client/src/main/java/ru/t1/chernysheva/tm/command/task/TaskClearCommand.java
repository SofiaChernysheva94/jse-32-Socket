package ru.t1.chernysheva.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.dto.request.task.TaskClearRequest;

public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-clear";

    @NotNull
    private final String DESCRIPTION = "Delete all tasks.";

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");

        @NotNull final TaskClearRequest request = new TaskClearRequest();

        getTaskEndpoint().clearTask(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
