package ru.t1.chernysheva.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.dto.request.data.*;
import ru.t1.chernysheva.tm.dto.response.data.*;
import ru.t1.chernysheva.tm.exception.AbstractException;

public interface IDomainEndpointClient extends IEndpointClient {

    @NotNull
    DataLoadBackupResponse loadDataBackup(@NotNull DataLoadBackupRequest request) throws AbstractException;

    @NotNull
    DataLoadBase64Response loadDataBase64(@NotNull DataLoadBase64Request request) throws AbstractException;

    @NotNull
    DataLoadBinaryResponse loadDataBinary(@NotNull DataLoadBinaryRequest request) throws AbstractException;

    @NotNull
    DataLoadJsonFasterXmlResponse loadDataJsonFasterXml(@NotNull DataLoadJsonFasterXmlRequest request) throws AbstractException;

    @NotNull
    DataLoadJsonJaxBResponse loadDataJsonJaxB(@NotNull DataLoadJsonJaxBRequest request) throws AbstractException;

    @NotNull
    DataLoadXmlFasterXmlResponse loadDataXmlFasterXml(@NotNull DataLoadXmlFasterXmlRequest request) throws AbstractException;

    @NotNull
    DataLoadXmlJaxBResponse loadDataXmlJaxB(@NotNull DataLoadXmlJaxBRequest request) throws AbstractException;

    @NotNull DataSaveJsonJaxBResponse saveDataXmlJaxB(@NotNull DataSaveJsonJaxBRequest request);

    @NotNull
    DataLoadYamlFasterXmlResponse loadDataYamlFasterXml(@NotNull DataLoadYamlFasterXmlRequest request) throws AbstractException;

    @NotNull
    DataSaveBackupResponse saveDataBackup(@NotNull DataSaveBackupRequest request) throws AbstractException;

    @NotNull
    DataSaveBase64Response saveDataBase64(@NotNull DataSaveBase64Request request) throws AbstractException;

    @NotNull
    DataSaveBinaryResponse saveDataBinary(@NotNull DataSaveBinaryRequest request) throws AbstractException;

    @NotNull
    DataSaveJsonFasterXmlResponse saveDataJsonFasterXml(@NotNull DataSaveJsonFasterXmlRequest request) throws AbstractException;

    @NotNull
    DataSaveJsonJaxBResponse saveDataJsonJaxB(@NotNull DataSaveJsonJaxBRequest request) throws AbstractException;

    @NotNull
    DataSaveXmlFasterXmlResponse saveDataXmlFasterXml(@NotNull DataSaveXmlFasterXmlRequest request) throws AbstractException;

    @NotNull
    DataSaveXmlJaxBResponse saveDataXmlJaxB(@NotNull DataSaveXmlJaxBRequest request) throws AbstractException;

    @NotNull
    DataSaveYamlFasterXmlResponse saveDataYamlFasterXml(@NotNull DataSaveYamlFasterXmlRequest request) throws AbstractException;

}
