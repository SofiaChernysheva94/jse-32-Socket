package ru.t1.chernysheva.tm.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.component.Bootstrap;
import ru.t1.chernysheva.tm.component.Server;

public abstract class AbstractServerTask implements Runnable {

    @NotNull
    protected Server server;

    public AbstractServerTask(@NotNull final Server server) {
        this.server = server;
    }

}
