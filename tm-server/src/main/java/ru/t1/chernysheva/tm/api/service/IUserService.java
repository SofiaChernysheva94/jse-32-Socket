package ru.t1.chernysheva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.enumerated.Role;
import ru.t1.chernysheva.tm.model.User;

public interface IUserService extends IService<User>{

    @Nullable
    User create(@Nullable final String login, @Nullable final String password);

    @Nullable
    User create(@Nullable final String login, @Nullable String password, @Nullable final String email);

    @NotNull
    User create(@Nullable final String login, @Nullable String password, @Nullable Role role);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    @Nullable User removeByLogin(@Nullable final String login);

    @Nullable
    User removeByEmail(@Nullable final String email);

    @Nullable User setPassword(@Nullable String id, @Nullable String password);

    @Nullable User updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName, @Nullable String email);

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @Nullable
    Boolean isEmailExist(@Nullable String email);

    @Nullable User lockUserByLogin(@Nullable String login);

    @Nullable User unlockUserByLogin(@Nullable String login);

}
