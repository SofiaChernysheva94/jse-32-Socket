package ru.t1.chernysheva.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.enumerated.Status;
import ru.t1.chernysheva.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    @Nullable Project updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @Nullable Project updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    Project changeProjectStatusById(@Nullable String userId, @Nullable String Id, @Nullable Status status);

    Project changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @Nullable
    Project create(@Nullable String userId, @Nullable String name);

    @Nullable
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

}
