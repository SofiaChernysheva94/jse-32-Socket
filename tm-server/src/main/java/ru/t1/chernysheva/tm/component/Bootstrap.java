package ru.t1.chernysheva.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.api.endpoint.*;
import ru.t1.chernysheva.tm.api.repository.IProjectRepository;
import ru.t1.chernysheva.tm.api.repository.ITaskRepository;
import ru.t1.chernysheva.tm.api.repository.IUserRepository;
import ru.t1.chernysheva.tm.api.service.*;
import ru.t1.chernysheva.tm.dto.request.ApplicationAboutRequest;
import ru.t1.chernysheva.tm.dto.request.ApplicationVersionRequest;
import ru.t1.chernysheva.tm.dto.request.data.*;
import ru.t1.chernysheva.tm.dto.request.project.*;
import ru.t1.chernysheva.tm.dto.request.task.*;
import ru.t1.chernysheva.tm.dto.request.user.*;
import ru.t1.chernysheva.tm.endpoint.*;
import ru.t1.chernysheva.tm.enumerated.Role;
import ru.t1.chernysheva.tm.enumerated.Status;
import ru.t1.chernysheva.tm.model.Project;
import ru.t1.chernysheva.tm.model.User;
import ru.t1.chernysheva.tm.repository.ProjectRepository;
import ru.t1.chernysheva.tm.repository.TaskRepository;
import ru.t1.chernysheva.tm.repository.UserRepository;
import ru.t1.chernysheva.tm.service.*;
import ru.t1.chernysheva.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService(propertyService);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final Server server = new Server(this);

    {
        server.registry(ApplicationAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ApplicationVersionRequest.class, systemEndpoint::getVersion);

        server.registry(DataLoadBase64Request.class, domainEndpoint::loadDataBase64);
        server.registry(DataSaveBase64Request.class, domainEndpoint::saveDataBase64);
        server.registry(DataLoadBinaryRequest.class, domainEndpoint::loadDataBinary);
        server.registry(DataSaveBinaryRequest.class, domainEndpoint::saveDataBinary);
        server.registry(DataLoadJsonFasterXmlRequest.class, domainEndpoint::loadDataJsonFasterXml);
        server.registry(DataSaveJsonFasterXmlRequest.class, domainEndpoint::saveDataJsonFasterXml);
        server.registry(DataLoadJsonJaxBRequest.class, domainEndpoint::loadDataJsonJaxB);
        server.registry(DataSaveJsonJaxBRequest.class, domainEndpoint::saveDataJsonJaxB);
        server.registry(DataLoadXmlFasterXmlRequest.class, domainEndpoint::loadDataXmlFasterXml);
        server.registry(DataSaveXmlFasterXmlRequest.class, domainEndpoint::saveDataXmlFasterXml);
        server.registry(DataLoadXmlJaxBRequest.class, domainEndpoint::loadDataXmlJaxB);
        server.registry(DataSaveXmlJaxBRequest.class, domainEndpoint::saveDataXmlJaxB);
        server.registry(DataLoadYamlFasterXmlRequest.class, domainEndpoint::loadDataYamlFasterXml);
        server.registry(DataSaveYamlFasterXmlRequest.class, domainEndpoint::saveDataYamlFasterXml);

        server.registry(ProjectChangeStatusByIdRequest.class, projectEndpoint::changeProjectStatusById);
        server.registry(ProjectChangeStatusByIndexRequest.class, projectEndpoint::changeProjectStatusByIndex);
        server.registry(ProjectClearRequest.class, projectEndpoint::clearProject);
        server.registry(ProjectCompleteByIdRequest.class, projectEndpoint::completeProjectById);
        server.registry(ProjectCompleteByIndexRequest.class, projectEndpoint::completeProjectByIndex);
        server.registry(ProjectCreateRequest.class, projectEndpoint::createProject);
        server.registry(ProjectListRequest.class, projectEndpoint::listProject);
        server.registry(ProjectRemoveByIdRequest.class, projectEndpoint::removeProjectById);
        server.registry(ProjectRemoveByIndexRequest.class, projectEndpoint::removeProjectByIndex);
        server.registry(ProjectShowByIdRequest.class, projectEndpoint::showProjectById);
        server.registry(ProjectShowByIndexRequest.class, projectEndpoint::showProjectByIndex);
        server.registry(ProjectStartByIdRequest.class, projectEndpoint::startProjectById);
        server.registry(ProjectStartByIndexRequest.class, projectEndpoint::startProjectByIndex);
        server.registry(ProjectUpdateByIdRequest.class, projectEndpoint::updateProjectById);
        server.registry(ProjectUpdateByIndexRequest.class, projectEndpoint::updateProjectByIndex);

        server.registry(TaskChangeStatusByIdRequest.class, taskEndpoint::changeTaskStatusById);
        server.registry(TaskChangeStatusByIndexRequest.class, taskEndpoint::changeTaskStatusByIndex);
        server.registry(TaskClearRequest.class, taskEndpoint::clearTask);
        server.registry(TaskCompleteByIdRequest.class, taskEndpoint::completeTaskById);
        server.registry(TaskCompleteByIndexRequest.class, taskEndpoint::completeTaskByIndex);
        server.registry(TaskCreateRequest.class, taskEndpoint::createTask);
        server.registry(TaskListRequest.class, taskEndpoint::listTask);
        server.registry(TaskRemoveByIdRequest.class, taskEndpoint::removeTaskById);
        server.registry(TaskRemoveByIndexRequest.class, taskEndpoint::removeTaskByIndex);
        server.registry(TaskShowByIdRequest.class, taskEndpoint::showTaskById);
        server.registry(TaskShowByIndexRequest.class, taskEndpoint::showTaskByIndex);
        server.registry(TaskShowByProjectIdRequest.class, taskEndpoint::showTaskByProjectId);
        server.registry(TaskStartByIdRequest.class, taskEndpoint::startTaskById);
        server.registry(TaskStartByIndexRequest.class, taskEndpoint::startTaskByIndex);
        server.registry(TaskUpdateByIdRequest.class, taskEndpoint::updateTaskById);
        server.registry(TaskUpdateByIndexRequest.class, taskEndpoint::updateTaskByIndex);
        server.registry(TaskBindToProjectRequest.class, taskEndpoint::bindTaskToProject);
        server.registry(TaskUnbindFromProjectRequest.class, taskEndpoint::unbindTaskFromProject);

        server.registry(UserChangePasswordRequest.class, userEndpoint::changeUserPassword);
        server.registry(UserLockRequest.class, userEndpoint::lockUser);
        server.registry(UserRegistryRequest.class, userEndpoint::registryUser);
        server.registry(UserRemoveRequest.class, userEndpoint::removeUser);
        server.registry(UserUnlockRequest.class, userEndpoint::unlockUser);
        server.registry(UserProfileRequest.class, userEndpoint::showUserProfile);
        server.registry(UserUpdateProfileRequest.class, userEndpoint::updateUserProfile);

    }

    private void initBackup() {
        backup.start();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public void start() {
        initPID();
        initDemoData();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
        server.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
        backup.stop();
        server.stop();
    }

    private void initDemoData() {
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);
        @NotNull final User test = userService.create("test", "test");

        projectService.create(admin.getId(), "Project1", "Description1");
        projectService.create(admin.getId(), "Project2", "Description2");

        projectService.create(test.getId(), "Project1", "Description1");
        projectService.create(test.getId(), "Project2", "Description2");

        taskService.create(admin.getId(), "Task1", "Description1");
        taskService.create(admin.getId(), "Task2", "Description2");
    }

}

