package ru.t1.chernysheva.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.request.AbstractUserRequest;
import ru.t1.chernysheva.tm.enumerated.Sort;
import ru.t1.chernysheva.tm.model.Project;
import ru.t1.chernysheva.tm.model.Task;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TaskListRequest extends AbstractUserRequest {

    @Nullable
    private Sort sortType;

    public TaskListRequest(@Nullable final Sort sortType) {
        this.sortType = sortType;
    }


}
