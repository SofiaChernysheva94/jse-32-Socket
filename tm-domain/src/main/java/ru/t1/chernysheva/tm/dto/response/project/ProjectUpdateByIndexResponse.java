package ru.t1.chernysheva.tm.dto.response.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.model.Project;

public class ProjectUpdateByIndexResponse extends AbstractProjectResponse {

    public ProjectUpdateByIndexResponse(@NotNull final Project project) {
        super(project);
    }

}
