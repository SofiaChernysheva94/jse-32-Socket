package ru.t1.chernysheva.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.request.AbstractRequest;
import ru.t1.chernysheva.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class UserRemoveRequest extends AbstractUserRequest {

    @Nullable
    private String login;

}
