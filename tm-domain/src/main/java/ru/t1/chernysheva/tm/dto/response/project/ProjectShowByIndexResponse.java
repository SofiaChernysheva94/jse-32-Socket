package ru.t1.chernysheva.tm.dto.response.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.model.Project;

public class ProjectShowByIndexResponse extends AbstractProjectResponse {

    public ProjectShowByIndexResponse(@NotNull final Project project) {
        super(project);
    }

}
