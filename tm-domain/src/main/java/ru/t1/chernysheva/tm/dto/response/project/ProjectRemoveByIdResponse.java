package ru.t1.chernysheva.tm.dto.response.project;

import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.response.user.AbstractUserResponse;
import ru.t1.chernysheva.tm.model.Project;

public class ProjectRemoveByIdResponse extends AbstractProjectResponse {

    public ProjectRemoveByIdResponse(@Nullable final Project project) {
        super(project);
    }

}
