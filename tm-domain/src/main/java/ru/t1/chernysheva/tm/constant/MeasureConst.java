package ru.t1.chernysheva.tm.constant;

public class MeasureConst {

    public static final long KB = 1024;

    public static final long MB = KB * 1024;

    public static final long GB = MB * 1024;

    public static final long TB = GB * 1024;

    private MeasureConst() {
    }

}
