package ru.t1.chernysheva.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.dto.request.user.*;
import ru.t1.chernysheva.tm.dto.response.user.*;

public interface IUserEndpoint {

        @NotNull
        UserLockResponse lockUser(@NotNull UserLockRequest request);

        @NotNull
        UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request);

        @NotNull
        UserRemoveResponse removeUser(@NotNull UserRemoveRequest request);

        @NotNull
        UserRegistryResponse registryUser(@NotNull UserRegistryRequest request);

        @NotNull
        UserChangePasswordResponse changeUserPassword(@NotNull UserChangePasswordRequest request);

        @NotNull
        UserProfileResponse showUserProfile(@NotNull final UserProfileRequest request);

        @NotNull
        UserUpdateProfileResponse updateUserProfile(@NotNull UserUpdateProfileRequest request);

}
