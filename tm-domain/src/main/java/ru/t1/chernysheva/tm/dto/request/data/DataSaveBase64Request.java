package ru.t1.chernysheva.tm.dto.request.data;

import lombok.NoArgsConstructor;
import ru.t1.chernysheva.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public class DataSaveBase64Request extends AbstractUserRequest {
}
