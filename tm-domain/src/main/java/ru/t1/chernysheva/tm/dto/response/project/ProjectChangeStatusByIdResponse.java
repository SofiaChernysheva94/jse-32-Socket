package ru.t1.chernysheva.tm.dto.response.project;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.model.Project;

@Getter
@Setter
public class ProjectChangeStatusByIdResponse extends AbstractProjectResponse {

    public ProjectChangeStatusByIdResponse(@NotNull final Project project) {
        super(project);
    }

}
