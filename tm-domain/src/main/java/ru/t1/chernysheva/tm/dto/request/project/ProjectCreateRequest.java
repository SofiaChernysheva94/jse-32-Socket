package ru.t1.chernysheva.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class ProjectCreateRequest extends AbstractUserRequest {

    @Nullable
    private String name;

    @Nullable
    private String description;

    public ProjectCreateRequest(@Nullable String name, @Nullable String description) {
        this.name = name;
        this.description = description;
    }

}
